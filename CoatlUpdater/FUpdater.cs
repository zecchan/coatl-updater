﻿using CoatlUpdater.Manifests;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoatlUpdater
{
    public partial class FUpdater : Form
    {
        public FUpdater()
        {
            InitializeComponent();

            try
            {
                if (File.Exists("logo.png"))
                {
                    using (var fi = File.OpenRead("logo.png"))
                    {
                        using (var ms = new MemoryStream())
                        {
                            fi.Seek(0, SeekOrigin.Begin);
                            fi.CopyTo(ms);

                            var bmp = new Bitmap(ms);
                            pSplash.Image = bmp;
                        }
                    }
                }
            }
            catch
            {

            }
        }

        LocalManifest localManifest { get; set; }
        RemoteManifest remoteManifest { get; set; }
        string appBasePath { get; set; }

        public string defaultManifest { get; set; } = "manifest.json";

        public void Stage01CheckManifest()
        {
            lbCurrentAction.Text = "Reading manifest file...";

            if (!File.Exists(defaultManifest))
            {
                MessageBox.Show("Cannot find manifest file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnExit.Text = "Close";
                var path = Path.Combine(appBasePath, localManifest.mainExecutable);
                btnStart.Visible = File.Exists(path);
                return;
            }
            try
            {
                var json = File.ReadAllText(defaultManifest);
                localManifest = Newtonsoft.Json.JsonConvert.DeserializeObject<LocalManifest>(json);
                if (localManifest == null) throw new Exception();
                if (string.IsNullOrWhiteSpace(localManifest?.appName)) throw new Exception();
                if (string.IsNullOrWhiteSpace(localManifest?.appId)) throw new Exception();
                if (string.IsNullOrWhiteSpace(localManifest?.updateUri)) throw new Exception();
                if (string.IsNullOrWhiteSpace(localManifest?.mainExecutable)) throw new Exception();
                if (string.IsNullOrWhiteSpace(localManifest?.installPath)) throw new Exception();
                if (!string.IsNullOrWhiteSpace(localManifest?.note)) lbNote.Text = localManifest.note;
                appBasePath = Path.GetFullPath(localManifest.installPath);
            }
            catch
            {
                MessageBox.Show("Manifest file is corrupted or invalid.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                lbCurrentAction.Text = "Update failed.";
                btnExit.Text = "Close";
                var path = Path.Combine(appBasePath, localManifest.mainExecutable);
                btnStart.Visible = File.Exists(path);
                return;
            }
            Text = "Coatl Updater - " + localManifest.appName;
            Stage02CheckRemoteManifest();
        }

        async void Stage02CheckRemoteManifest()
        {
            Invoke(new Action(() =>
            {
                lbCurrentAction.Text = "Checking for update...";
            }));
            UpdateOverallProgressBar(10);
            try
            {
                HttpClient clnt = new HttpClient();
                var rmjson = await clnt.GetAsync(localManifest.updateUri + "?appid=" + localManifest.appId);
                if (rmjson.StatusCode != System.Net.HttpStatusCode.OK)
                    throw new Exception("Failed to get update list from the server.");

                var json = await rmjson.Content.ReadAsStringAsync();
                remoteManifest = Newtonsoft.Json.JsonConvert.DeserializeObject<RemoteManifest>(json);
                if (remoteManifest == null) throw new Exception("Failed to get update list: server returned invalid data.");
                if (string.IsNullOrWhiteSpace(remoteManifest?.appId)) throw new Exception("Failed to get update list: server returned invalid data.");
                if (string.IsNullOrWhiteSpace(remoteManifest?.appName)) throw new Exception("Failed to get update list: server returned invalid data.");
                if (string.IsNullOrWhiteSpace(remoteManifest?.version)) throw new Exception("Failed to get update list: server returned invalid data.");
                if (remoteManifest?.appFiles == null) throw new Exception("Failed to get update list: server returned invalid data.");
                if (remoteManifest.appId.ToLower() != localManifest.appId.ToLower()) throw new Exception("Failed to get update list: server returned invalid data.");

                Text += " v" + remoteManifest.version;

                Invoke(new Action(() =>
                {
                    Stage03CheckLocalFiles();
                }));
            }
            catch (Exception ex)
            {
                Invoke(new Action(() =>
                {
                    if (MessageBox.Show(ex.Message, "Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry)
                        Stage02CheckRemoteManifest();
                    else
                    {
                        lbCurrentAction.Text = "Update failed.";
                        btnExit.Text = "Close";
                        var path = Path.Combine(appBasePath, localManifest.mainExecutable);
                        btnStart.Visible = File.Exists(path);
                    }
                }));
                return;
            }
        }

        List<FileHashListItem> shouldUpdate { get; } = new List<FileHashListItem>();
        List<string> deleteList { get; } = new List<string>();
        async void Stage03CheckLocalFiles()
        {
            Invoke(new Action(() =>
            {
                lbCurrentAction.Text = "Checking local files...";
            }));
            UpdateOverallProgressBar(20);

            await Task.Run(new Action(() =>
            {
                try
                {
                    List<FileHashListItem> listItems = new List<FileHashListItem>();
                    var deleteItems = new List<string>();
                    UpdateActionProgressBar("Checking files...", 0);
                    double mx = remoteManifest.appFiles.Count;
                    var cn = 0;
                    foreach (var f in remoteManifest.appFiles)
                    {
                        cn++;
                        UpdateOverallProgressBar(40 + (int)Math.Round(cn / mx * 20));
                        UpdateActionProgressBar("Checking file: " + f.filePath, (int)Math.Round(cn / mx * 100));
                        var fname = Path.Combine(appBasePath, f.filePath);
                        if (!File.Exists(fname))
                        {
                            if (f.shouldNotExist) continue;
                            else
                            {
                                listItems.Add(f);
                                continue;
                            }
                        }
                        else
                        {
                            if (f.shouldNotExist)
                            {
                                deleteItems.Add(fname);
                                continue;
                            }
                        }
                        using (var md5 = System.Security.Cryptography.MD5.Create())
                        {
                            using (var stream = File.OpenRead(fname))
                            {
                                var hashResult = md5.ComputeHash(stream);
                                var md5hash = BitConverter.ToString(hashResult).Replace("-", "").ToLower();
                                if (md5hash != f.hashMD5.ToLower().Trim())
                                {
                                    listItems.Add(f);
                                }
                            }
                        }
                        // at this point, the file is newest, no need to update
                    }
                    UpdateActionProgressBar("File check completed.", 100);
                    Invoke(new Action(() =>
                    {
                        shouldUpdate.AddRange(listItems);
                        deleteList.AddRange(deleteItems);

                        if (shouldUpdate.Count == 0 && deleteList.Count == 0)
                        {
                            // no update
                            UpdateOverallProgressBar(100);
                            UpdateActionProgressBar("", 100);
                            lbCurrentAction.Text = "Application is up to date. (" + remoteManifest.appName + " v" + remoteManifest.version + ")";
                            btnExit.Enabled = true;
                            btnExit.Text = "OK";
                            var path = Path.Combine(appBasePath, localManifest.mainExecutable);
                            btnStart.Visible = File.Exists(path);
                            return;
                        }

                        Stage04DownloadUpdates();
                    }));
                }
                catch (Exception ex)
                {
                    Invoke(new Action(() =>
                    {
                        MessageBox.Show("Error while checking local file: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        lbCurrentAction.Text = "Update failed.";
                        btnExit.Text = "Close";
                        var path = Path.Combine(appBasePath, localManifest.mainExecutable);
                        btnStart.Visible = File.Exists(path);
                    }));
                    return;
                }
            }));
        }

        async void Stage04DownloadUpdates()
        {
            Invoke(new Action(() =>
            {
                lbCurrentAction.Text = "Downloading updates...";
            }));
            UpdateOverallProgressBar(40);
            UpdateActionProgressBar("Downloading update files...", 0);
            try
            {
                var httpClient = new HttpClient();
                double max = shouldUpdate.Count;
                var cnt = 0;
                if (Directory.Exists("temp"))
                    Directory.Delete("temp", true);
                Directory.CreateDirectory("temp");
                foreach (var f in shouldUpdate)
                {
                    var dlpath = Path.Combine(localManifest.updateUri, "get") + "?appid=" + localManifest.appId + "&file=" + (f.filePath).Replace("\\", "/");
                    cnt++;
                    UpdateOverallProgressBar(40 + (int)Math.Round(cnt / max * 40));
                    UpdateActionProgressBar("Downloading '" + f.filePath + "'... (" + (Math.Round(f.size / 1000000d, 2)) + " KB - " + cnt + " of " + max + ")", (int)Math.Round(cnt / max * 100));
                    while (true)
                    {
                        try
                        {
                            var res = await httpClient.GetAsync(dlpath);
                            if (!res.IsSuccessStatusCode)
                            {
                                throw new Exception(res.StatusCode.ToString());
                            }
                            else
                            {
                                // save it somewhere
                                var outpath = "temp/" + f.filePath;
                                var dir = Path.GetDirectoryName(outpath);
                                if (!Directory.Exists(dir))
                                    Directory.CreateDirectory(dir);
                                var strm = await res.Content.ReadAsStreamAsync();
                                using (var fo = File.Create(outpath))
                                {
                                    strm.CopyTo(fo);
                                }
                                break;
                            }
                        }
                        catch (Exception ex)
                        {
                            if (MessageBox.Show("Error while downloading update file: " + f.filePath + "\r\nError: " + ex.Message, "Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) != DialogResult.Retry)
                            {
                                Directory.Delete("temp", true);
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                UpdateActionProgressBar("Download completed.", 100);
                Invoke(new Action(() =>
                {
                    Stage05CopyFiles();
                }));
            }
            catch (Exception ex)
            {
                Invoke(new Action(() =>
                {
                    MessageBox.Show("Error while downloading update: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lbCurrentAction.Text = "Update failed.";
                    btnExit.Text = "Close";
                    var path = Path.Combine(appBasePath, localManifest.mainExecutable);
                    btnStart.Visible = File.Exists(path);
                }));
                return;
            }
        }

        async void Stage05CopyFiles()
        {
            Invoke(new Action(() =>
            {
                lbCurrentAction.Text = "Installing updates...";
                btnExit.Enabled = false;
            }));
            UpdateOverallProgressBar(80);
            UpdateActionProgressBar("Copying update files...", 0);
            await Task.Run(new Action(() =>
            {
                try
                {
                    foreach (var d in deleteList)
                    {
                        try
                        {
                            if (File.Exists(d)) File.Delete(d);
                        }
                        catch { }
                    }
                    double max = shouldUpdate.Count;
                    var cnt = 0;
                    foreach (var f in shouldUpdate)
                    {
                        cnt++;
                        UpdateOverallProgressBar(80 + (int)Math.Round(cnt / max * 20));
                        UpdateActionProgressBar("Copying update file '" + f.filePath + "'...", (int)Math.Round(cnt / max * 100));
                        var srcPath = Path.Combine("temp", f.filePath);
                        var dstPath = Path.Combine(appBasePath, f.filePath);
                        while (File.Exists(dstPath))
                        {
                            try
                            {
                                File.Delete(dstPath);
                            }
                            catch (Exception exx)
                            {
                                if (MessageBox.Show("Error while trying to update file: " + f.filePath + "\r\nError: " + exx.Message, "Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) != DialogResult.Retry)
                                {
                                    throw new Exception("Cannot remove file: " + f.filePath);
                                }
                            }
                        }
                        while (true)
                        {
                            try
                            {
                                var dstDir = Path.GetDirectoryName(dstPath);
                                if (!Directory.Exists(dstDir)) Directory.CreateDirectory(dstDir);
                                File.Copy(srcPath, dstPath);
                                break;
                            }
                            catch (Exception exx)
                            {
                                if (MessageBox.Show("Error while trying to update file: " + f.filePath + "\r\nError: " + exx.Message, "Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) != DialogResult.Retry)
                                {
                                    throw new Exception("Cannot write file: " + f.filePath);
                                }
                            }
                        }
                        Invoke(new Action(() =>
                        {
                            UpdateActionProgressBar("", 100);
                            lbCurrentAction.Text = "Update completed! " + remoteManifest.appName + " is updated to v" + remoteManifest.version;
                            btnExit.Enabled = true;
                            var path = Path.Combine(appBasePath, localManifest.mainExecutable);
                            btnStart.Visible = File.Exists(path);
                            btnExit.Text = "OK";
                        }));
                    }
                }
                catch (Exception ex)
                {
                    Invoke(new Action(() =>
                    {
                        lbCurrentAction.Text = "Update failed.";
                        MessageBox.Show("Error while updating: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        var path = Path.Combine(appBasePath, localManifest.mainExecutable);
                        btnStart.Visible = File.Exists(path);
                        btnExit.Enabled = true;
                        btnExit.Text = "Close";
                    }));
                    return;
                }
                finally
                {
                    try
                    {

                        if (Directory.Exists("temp"))
                            Directory.Delete("temp", true);
                    }
                    catch { }
                }
            }));
        }

        private void UpdateOverallProgressBar(int percent)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    lbOverall.Text = "Overall: " + percent + "%";
                    pbOverall.Value = percent;
                }));
            }
            else
            {
                lbOverall.Text = "Overall: " + percent + "%";
                pbOverall.Value = percent;
            }
        }
        private void UpdateActionProgressBar(string action, int percent)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    lbAction.Text = action;
                    pbAction.Value = percent;
                }));
            }
            else
            {
                lbAction.Text = action;
                pbAction.Value = percent;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FUpdater_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!btnExit.Enabled)
            {
                e.Cancel = true;
            }
            if (btnExit.Text == "Cancel")
            {
                if (MessageBox.Show("Are you sure to cancel the update?\r\nWARNING: The application may no longer work.", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                    e.Cancel = true;
            }
            if (btnExit.Text == "OK")
            {
            }
        }

        private void FUpdater_Shown(object sender, EventArgs e)
        {
            Stage01CheckManifest();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var path = Path.Combine(appBasePath, localManifest.mainExecutable);
            try
            {
                var psi = new ProcessStartInfo()
                {
                    FileName = path,
                    WorkingDirectory = Path.GetDirectoryName(path)
                };
                Process.Start(psi);
                Close();
            }
            catch { }
        }
    }
}

