﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoatlUpdater.Manifests
{
    public class RemoteManifest
    {
        public string appName { get; set; }

        public string appId { get; set; }

        public string version { get; set; }

        public List<FileHashListItem> appFiles { get; set; }
    }

    public class FileHashListItem
    {
        public string filePath { get; set; }

        public bool shouldNotExist { get; set; }

        public string hashMD5 { get; set; }

        public long size { get; set; }
    }
}
