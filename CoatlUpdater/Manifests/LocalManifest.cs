﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoatlUpdater.Manifests
{
    public class LocalManifest
    {
        public string appName { get; set; }

        public string appId { get; set; }

        public string updateUri { get; set; }

        public string mainExecutable { get; set; }

        public string installPath { get; set; }

        public string note { get; set; }
    }
}
