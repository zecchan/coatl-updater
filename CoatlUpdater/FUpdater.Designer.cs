﻿
namespace CoatlUpdater
{
    partial class FUpdater
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FUpdater));
            this.pSplash = new System.Windows.Forms.PictureBox();
            this.lbNote = new System.Windows.Forms.Label();
            this.lbCurrentAction = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbOverall = new System.Windows.Forms.ProgressBar();
            this.lbOverall = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pbAction = new System.Windows.Forms.ProgressBar();
            this.lbAction = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.lbETA = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pSplash)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // pSplash
            // 
            this.pSplash.Dock = System.Windows.Forms.DockStyle.Top;
            this.pSplash.Location = new System.Drawing.Point(0, 0);
            this.pSplash.Name = "pSplash";
            this.pSplash.Size = new System.Drawing.Size(472, 150);
            this.pSplash.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pSplash.TabIndex = 0;
            this.pSplash.TabStop = false;
            // 
            // lbNote
            // 
            this.lbNote.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbNote.Location = new System.Drawing.Point(0, 150);
            this.lbNote.Name = "lbNote";
            this.lbNote.Padding = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.lbNote.Size = new System.Drawing.Size(472, 46);
            this.lbNote.TabIndex = 1;
            this.lbNote.Text = "Please wait while the updates are being downloaded and installed.\r\nDO NOT turn of" +
    "f your computer or accessing the application.";
            this.lbNote.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbCurrentAction
            // 
            this.lbCurrentAction.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbCurrentAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCurrentAction.Location = new System.Drawing.Point(0, 196);
            this.lbCurrentAction.Name = "lbCurrentAction";
            this.lbCurrentAction.Padding = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbCurrentAction.Size = new System.Drawing.Size(472, 36);
            this.lbCurrentAction.TabIndex = 2;
            this.lbCurrentAction.Text = "Downloading updates...";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pbOverall);
            this.panel1.Controls.Add(this.lbOverall);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 232);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.panel1.Size = new System.Drawing.Size(472, 61);
            this.panel1.TabIndex = 3;
            // 
            // pbOverall
            // 
            this.pbOverall.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbOverall.Location = new System.Drawing.Point(8, 28);
            this.pbOverall.Name = "pbOverall";
            this.pbOverall.Size = new System.Drawing.Size(456, 23);
            this.pbOverall.TabIndex = 4;
            // 
            // lbOverall
            // 
            this.lbOverall.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbOverall.Location = new System.Drawing.Point(8, 0);
            this.lbOverall.Name = "lbOverall";
            this.lbOverall.Padding = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.lbOverall.Size = new System.Drawing.Size(456, 28);
            this.lbOverall.TabIndex = 5;
            this.lbOverall.Text = "Overall: 0%";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pbAction);
            this.panel2.Controls.Add(this.lbAction);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 293);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.panel2.Size = new System.Drawing.Size(472, 61);
            this.panel2.TabIndex = 4;
            // 
            // pbAction
            // 
            this.pbAction.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbAction.Location = new System.Drawing.Point(8, 28);
            this.pbAction.Name = "pbAction";
            this.pbAction.Size = new System.Drawing.Size(456, 23);
            this.pbAction.TabIndex = 4;
            // 
            // lbAction
            // 
            this.lbAction.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbAction.Location = new System.Drawing.Point(8, 0);
            this.lbAction.Name = "lbAction";
            this.lbAction.Padding = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.lbAction.Size = new System.Drawing.Size(456, 28);
            this.lbAction.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnStart);
            this.panel3.Controls.Add(this.btnExit);
            this.panel3.Controls.Add(this.lbETA);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 354);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(472, 64);
            this.panel3.TabIndex = 5;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(341, 12);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(119, 40);
            this.btnExit.TabIndex = 7;
            this.btnExit.Text = "Cancel";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lbETA
            // 
            this.lbETA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbETA.Location = new System.Drawing.Point(0, 0);
            this.lbETA.Name = "lbETA";
            this.lbETA.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.lbETA.Size = new System.Drawing.Size(472, 64);
            this.lbETA.TabIndex = 6;
            this.lbETA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(12, 12);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(119, 40);
            this.btnStart.TabIndex = 8;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Visible = false;
            this.btnStart.Click += new System.EventHandler(this.button1_Click);
            // 
            // FUpdater
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 418);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lbCurrentAction);
            this.Controls.Add(this.lbNote);
            this.Controls.Add(this.pSplash);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FUpdater";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Coatl Updater";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FUpdater_FormClosing);
            this.Shown += new System.EventHandler(this.FUpdater_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pSplash)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pSplash;
        private System.Windows.Forms.Label lbNote;
        private System.Windows.Forms.Label lbCurrentAction;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ProgressBar pbOverall;
        private System.Windows.Forms.Label lbOverall;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ProgressBar pbAction;
        private System.Windows.Forms.Label lbAction;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label lbETA;
        private System.Windows.Forms.Button btnStart;
    }
}

