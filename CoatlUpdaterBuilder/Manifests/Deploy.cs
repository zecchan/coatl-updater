﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using FluentFTP;

namespace CoatlUpdaterBuilder.Manifests
{
    public class Deploy
    {
        public string method { get; set; } = "FTP";
        public string username { get; set; }
        public string password { get; set; }
        public string host { get; set; }
        public string path { get; set; } = "/";
        public string hashProvider { get; set; }

        public bool HasPassword { get => !string.IsNullOrWhiteSpace(password); }
    }

    public class Deployer
    {
        public void Deploy(Deploy deployConfiguration, string path)
        {
            if (deployConfiguration == null)
                throw new ArgumentNullException(nameof(deployConfiguration));
            if (deployConfiguration.method != "FTP")
                throw new Exception("Unknown method " + deployConfiguration.method);

            Console.WriteLine("Deploying via '" + deployConfiguration.method + "' to " + deployConfiguration.host + "...");

            var files = Directory.EnumerateFiles(path, "*", SearchOption.AllDirectories);
            bool serverSupportHash = true;
            using (var ftp = new FtpClient())
            {
                try
                {
                    Console.Write("Connecting...");
                    ftp.Connect(new FtpProfile()
                    {
                        Credentials = new NetworkCredential(deployConfiguration.username, deployConfiguration.password),
                        DataConnection = FtpDataConnectionType.AutoPassive,
                        Encoding = Encoding.UTF8,
                        Host = deployConfiguration.host,
                        RetryAttempts = 3
                    });
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Success");
                    Console.ResetColor();
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Failed");
                    Console.WriteLine(ex.Message);
                    Console.ResetColor();
                    throw ex;
                }
                foreach (var f in files)
                {
                    try
                    {
                        var relpath = f;
                        if (relpath.ToLower().StartsWith(path.ToLower()))
                            relpath = relpath.Substring(path.Length + 1);
                        relpath = relpath.Replace("\\", "/");
                        var destpath = Path.Combine(deployConfiguration.path, relpath).Replace("\\", "/");

                        var destdir = Path.GetDirectoryName(destpath);
                        if (!ftp.DirectoryExists(destdir))
                        {
                            Console.WriteLine("Directory '" + destdir + "' does not exists, creating...");
                            ftp.CreateDirectory(destdir, true);
                        }

                        var shouldskip = false;
                        if (ftp.FileExists(destpath))
                        {
                            string lMD5 = null;
                            try
                            {
                                using (var md5 = MD5.Create())
                                {
                                    using (var fi = File.OpenRead(f))
                                    {
                                        var hbuf = md5.ComputeHash(fi);
                                        lMD5 = BitConverter.ToString(hbuf).Replace("-", "").ToLower();
                                    }
                                }
                            }
                            catch { }

                            if (lMD5 != null)
                            {
                                if (serverSupportHash)
                                    Console.Write("Checking '" + relpath + "' using FTP cheksum...");
                                var hash = serverSupportHash ? ftp.GetChecksum(destpath, FtpHashAlgorithm.MD5) : null;

                                if (hash != null && hash.IsValid && hash.Algorithm == FtpHashAlgorithm.MD5)
                                {
                                    var rMD5 = hash.Value.ToLower().Trim();
                                    if (rMD5 == lMD5)
                                    {
                                        shouldskip = true;
                                        Console.ForegroundColor = ConsoleColor.Green;
                                        Console.WriteLine("Unchanged - Skipped");
                                        Console.ResetColor();
                                    }
                                    else
                                    {
                                        Console.ForegroundColor = ConsoleColor.Blue;
                                        Console.WriteLine("Changed");
                                        Console.ResetColor();
                                    }
                                }
                                else
                                {
                                    if (serverSupportHash)
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("Not supported");
                                        Console.ResetColor();
                                    }
                                    serverSupportHash = false;
                                    if (!string.IsNullOrWhiteSpace(deployConfiguration.hashProvider))
                                    {
                                        Console.Write("Checking '" + relpath + "' using HashProvider...");
                                        try
                                        {
                                            string rMD5 = null;
                                            using (var wc = new WebClient())
                                            {
                                                var uri = string.Format(deployConfiguration.hashProvider, relpath);
                                                rMD5 = wc.DownloadString(uri);
                                            }
                                            if (rMD5 != null) rMD5 = rMD5.ToLower().Trim();
                                            if (rMD5 == lMD5)
                                            {
                                                shouldskip = true;
                                                Console.ForegroundColor = ConsoleColor.Green;
                                                Console.WriteLine("Unchanged - Skipped");
                                                Console.ResetColor();
                                            }
                                            else
                                            {
                                                Console.ForegroundColor = ConsoleColor.Blue;
                                                Console.WriteLine("Changed");
                                                Console.ResetColor();
                                            }
                                        }
                                        catch(Exception exx)
                                        {
                                            Console.ForegroundColor = ConsoleColor.Red;
                                            Console.WriteLine("Failed");
                                            Console.ResetColor();
                                        }
                                    }
                                }
                            }
                        }

                        if (!shouldskip)
                        {
                            Console.Write("Uploading '" + relpath + "'...");
                            var resp = ftp.UploadFile(f, destpath);

                            if (resp == FtpStatus.Failed)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Failed");
                                Console.ResetColor();
                            }
                            else if (resp == FtpStatus.Skipped)
                            {
                                Console.ForegroundColor = ConsoleColor.Gray;
                                Console.WriteLine("Skipped");
                                Console.ResetColor();
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("Success");
                                Console.ResetColor();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Failed");
                        Console.WriteLine(ex.Message);
                        Console.ResetColor();
                        throw ex;
                    }
                }
            }


        }
    }
}
