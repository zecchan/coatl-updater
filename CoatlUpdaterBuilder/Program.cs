﻿using CoatlUpdaterBuilder.Manifests;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoatlUpdaterBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            var dirArg = args != null && args.Length > 0 ? args[0] : null;
            Console.Write("Welcome to ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Coatl Updater Builder (CAU+) v1.1");
            Console.ResetColor();

            string path = null;
            if (dirArg != null && Directory.Exists(dirArg))
            {
                path = dirArg;
            }
            else
            {
                Console.WriteLine("");
                Console.Write("Please enter the application directory: ");
                path = Console.ReadLine();
            }

            if (!Directory.Exists(path))
            {
                ErrorExit("Cannot find folder '" + path + "'");
                return;
            }

            var bmpath = Path.Combine(path, "basemanifest.json");
            if (!File.Exists(bmpath))
            {
                ErrorExit("'basemanifest.json' is missing.");
                return;
            }

            RemoteManifest rman;
            try
            {
                var json = File.ReadAllText(bmpath);
                rman = Newtonsoft.Json.JsonConvert.DeserializeObject<RemoteManifest>(json);
                if (rman == null) throw new Exception();

                if (string.IsNullOrWhiteSpace(rman.appId)) throw new Exception();
                if (string.IsNullOrWhiteSpace(rman.appName)) throw new Exception();
                if (string.IsNullOrWhiteSpace(rman.version)) throw new Exception();
                rman.appFiles = new List<FileHashListItem>();
            }
            catch
            {
                ErrorExit("'basemanifest.json' is invalid.");
                return;
            }

            Console.WriteLine("Analyzing [" + rman.appName + " v" + rman.version + "]...");

            var srcdir = Path.Combine(path, "src");
            if (!Directory.Exists(srcdir))
            {
                ErrorExit("Source directory '" + srcdir + "' is not found.");
                return;
            }
            Console.WriteLine();

            var delpath = Path.Combine(path, "delete.txt");
            if (!File.Exists(delpath))
            {
                Console.WriteLine("WARNING: 'delete.txt' is not found in app dir. No file will be deleted on update.");
                Console.WriteLine();
            }

            var files = Directory.EnumerateFiles(srcdir, "*.*", SearchOption.AllDirectories);
            var fsrcpath = Path.GetFullPath(srcdir).ToLower() + "\\";
            var dic = new Dictionary<string, string>();
            foreach (var f in files)
            {
                var relpath = Path.GetFullPath(f);
                if (relpath.ToLower().StartsWith(fsrcpath))
                    relpath = relpath.Substring(fsrcpath.Length);

                if (rman.exclude != null && rman.exclude.Any(x => x != null && x.ToLower() == relpath.ToLower()))
                {
                    Console.WriteLine("'" + relpath + "' - excluded");
                    continue;
                }

                dic.Add(relpath, f);

                Console.Write("'" + relpath + "' :: ");
                var fi = new FileInfo(f);
                var af = new FileHashListItem()
                {
                    filePath = relpath,
                    hashMD5 = "",
                    size = fi.Length,
                    shouldNotExist = false
                };
                rman.appFiles.Add(af);

                using (var md5 = System.Security.Cryptography.MD5.Create())
                {
                    using (var stream = File.OpenRead(f))
                    {
                        var hashResult = md5.ComputeHash(stream);
                        af.hashMD5 = BitConverter.ToString(hashResult).Replace("-", "").ToLower();
                    }
                }
                Console.WriteLine(af.hashMD5 + " - added");
            }

            if (File.Exists(delpath))
            {
                try
                {
                    using (var fi = File.OpenText(delpath))
                    {
                        var line = fi.ReadLine();
                        while (line != null)
                        {
                            if (!string.IsNullOrWhiteSpace(line))
                            {
                                Console.WriteLine("'" + line + "' - flagged for deletion");
                                var af = new FileHashListItem()
                                {
                                    filePath = line?.Trim(),
                                    hashMD5 = "",
                                    size = 0,
                                    shouldNotExist = true
                                };
                                rman.appFiles.Add(af);
                            }
                            line = fi.ReadLine();
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorExit(ex.Message);
                    return;
                }
            }

            var temppath = "temp";
            // copy files to temp 
            while (Directory.Exists(temppath))
            {
                temppath = "temp_" + Guid.NewGuid().ToString("n");
            }
            Directory.CreateDirectory(temppath);
            var di = new DirectoryInfo(temppath);
            di.Attributes |= FileAttributes.Hidden;
            try
            {
                using (var fo = File.CreateText(temppath + "/manifest.json"))
                {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(rman);
                    fo.Write(json);
                }
                foreach (var kv in dic)
                {
                    var dstPath = Path.Combine(temppath, kv.Key);
                    var dstDir = Path.GetDirectoryName(dstPath);
                    if (!Directory.Exists(dstDir))
                        Directory.CreateDirectory(dstDir);
                    File.Copy(kv.Value, dstPath);
                }

                // zip it
                Console.WriteLine("Creating archive for updater...");
                if (File.Exists(rman.appId + ".zip"))
                    File.Delete(rman.appId + ".zip");
                ZipFile.CreateFromDirectory(temppath, rman.appId + ".zip");
                ShowFileInExplorer(Path.GetFullPath(rman.appId + ".zip"));
                Console.WriteLine("Update definition created, archive created in: ");
                Console.Write("'");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write(Path.GetFullPath(rman.appId + ".zip"));
                Console.ResetColor();
                Console.WriteLine("'");

                // deploy
                try
                {
                    var dppath = Path.Combine(path, "deploy.json");
                    if (File.Exists(dppath))
                    {
                        Console.WriteLine();
                        Console.WriteLine("Starting deploy script...");

                        var json = File.ReadAllText(dppath);
                        var deploy = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Deploy>>(json);
                        if (deploy != null)
                        {
                            var dp = new Deployer();
                            var ctr = 1;
                            foreach (var d in deploy)
                            {
                                Console.WriteLine();
                                Console.WriteLine("Running Deploy #" + ctr + ":");
                                try
                                {
                                    dp.Deploy(d, Path.GetFullPath(temppath));
                                    Console.WriteLine("Deploy completed successfully");
                                }
                                catch (Exception ex2)
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Deploy failed: " + ex2.Message);
                                    Console.ResetColor();
                                }
                                ctr++;
                            }
                        }
                        else
                            Console.WriteLine("Invalid deploy script");
                    }
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Deploy failed: " + ex.Message);
                    Console.ResetColor();
                }
            }
            finally
            {
                try
                {
                    Directory.Delete(temppath, true);
                }
                catch { }
            }

            Console.WriteLine("");
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        static void ErrorExit(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Error: " + message);
            Console.ResetColor();
            Console.WriteLine("");
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
        private static void ShowFileInExplorer(string filePath)
        {
            try
            {
                var winDir = Environment.GetEnvironmentVariable("windir");
                if (winDir != null)
                {
                    var explorerPath = Path.Combine(winDir, @"explorer.exe");
                    var arguments = String.Format("/select, {0}{1}{0}", (char)34, filePath);
                    Process.Start(explorerPath, arguments);
                }
            }
            catch (Exception ex)
            {
                //handle the exception your way!
            }
        }
    }
}
